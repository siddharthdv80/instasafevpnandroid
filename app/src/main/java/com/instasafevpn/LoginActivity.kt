package com.instasafevpn

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.instasafevpn.device_info.DeviceInfo
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    val URL_INSTASAFE = "http://dev.instasafe.io/#/login"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initViews()
    }

    private fun initViews() {
        webview!!.settings.javaScriptEnabled = true
        webview!!.settings.javaScriptCanOpenWindowsAutomatically = true
        webview!!.settings.allowFileAccess = true
        webview!!.settings.domStorageEnabled = true
        webview!!.clearCache(true)
        webview!!.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                progressBar!!.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressBar!!.visibility = View.GONE
            }

            override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
                println(">>URL ::" + request!!.url)
                println(">>Method ::" + request.method)
                println(">>headers ::" + request.requestHeaders)
                if (request.url.toString().contains("dev.instasafe.io/console/tenant/")) {
                    showToken(request.requestHeaders)
                }
                return super.shouldInterceptRequest(view, request)
            }
        }
        webview!!.loadUrl(URL_INSTASAFE)

        button!!.setOnClickListener {
            DeviceInfo.showDeviceInfo(this)
        }
    }

    private fun showToken(requestHeaders: Map<String, String>) {
        val token = requestHeaders["Authorization"]
        println(">>Token ::$token")
        runOnUiThread {
            Toast.makeText(this, token, Toast.LENGTH_LONG).show()
            finish()
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
