package com.instasafevpn.application;

import android.app.Application;

import com.instasafevpn.events.OnVPNStatusUpdate;
import com.instasafevpn.wirevpn.VPNConfig;
import com.instasafevpn.wirevpn.WireTunnel;
import com.wireguard.android.backend.Backend;
import com.wireguard.android.backend.GoBackend;
import com.wireguard.android.backend.Tunnel;

import org.greenrobot.eventbus.EventBus;

public class InstaSafeApplication extends Application {
    private static InstaSafeApplication sApplication;
    private Backend mVPNBackend;
    private WireTunnel wireTunnel;

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        mVPNBackend = new GoBackend(this);
    }

    public static InstaSafeApplication getInstance() {
        return sApplication;
    }

    public Backend getVPNBackend() {
        if (mVPNBackend == null) {
            mVPNBackend = new GoBackend(this);
        }
        return mVPNBackend;
    }
}
