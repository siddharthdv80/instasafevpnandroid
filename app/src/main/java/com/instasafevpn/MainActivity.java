package com.instasafevpn;

import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.instasafevpn.certification.CSRHelper;
import com.instasafevpn.events.OnVPNStatusUpdate;
import com.instasafevpn.events.VPNState;
import com.instasafevpn.service.InstaSafeVpnService;
import com.instasafevpn.wirevpn.TunnelManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.spongycastle.pkcs.PKCS10CertificationRequest;

import java.security.KeyPair;
import java.security.KeyPairGenerator;

public class MainActivity extends AppCompatActivity {
    private TextView tvStatus;
    private Button btnConnect;
    private TextView tvGateWay;
    private TextView tvController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvStatus = findViewById(R.id.tv_status);
        btnConnect = findViewById(R.id.btn_connect);
        tvGateWay = findViewById(R.id.tv_gateway);
        tvController = findViewById(R.id.tv_controller);
        tvGateWay.setText("10.50.0.3/32");
        tvController.setText("3.101.128.28:443");
        tvStatus.setText(getString(VPNState.STATE_DISCONNECTED.getStringID()));
        btnConnect.setOnClickListener(v -> {
            if (!TunnelManager.getInstance().isVPNConnected()) {
                //If not running then prepare service
                updateUI(VPNState.STATE_CONNECTING);
                prepareVPNService();
            } else {
                //else stop it
                updateUI(VPNState.STATE_DISCONNECTED);
                TunnelManager.getInstance().startStopVPN();
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVPNStatusUpdate(OnVPNStatusUpdate event) {
        VPNState state = event.getState();
        updateUI(state);
        if (state == VPNState.STATE_REATTEMPT) {
            prepareVPNService();
        }
    }

    private void updateUI(VPNState newState) {
        runOnUiThread(() -> {
            int statusStringId = newState.getStringID();
            if (statusStringId != -1 && tvStatus != null) {
                tvStatus.setText(getString(statusStringId));
            }
            updateConnectButton(newState);
        });
    }

    private void updateConnectButton(VPNState newState) {
        //Keep connect button disabled to avoid multiple actions
        if (newState == VPNState.STATE_CONNECTED || newState == VPNState.STATE_DISCONNECTED ||
                newState == VPNState.STATE_VPN_FAILED) {
            btnConnect.setEnabled(true);
        } else {
            btnConnect.setEnabled(false);
        }

        if (newState == VPNState.STATE_CONNECTED) {
            btnConnect.setText("Disconnect");
        } else {
            btnConnect.setText("Connect");
        }
    }

    private void prepareVPNService() {
        Intent intent = VpnService.prepare(MainActivity.this);
        if (intent != null) {
            startActivityForResult(intent, 0);
        } else {
            onActivityResult(0, RESULT_OK, null);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            TunnelManager.getInstance().startStopVPN();
//            startService(getServiceIntent());
        }
    }

    private Intent getServiceIntent() {
        return new Intent(this, MyVPNService.class);
    }


    private void generateKeyGen() {
        try {
            //keypair
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            byte[] publicKey = kp.getPublic().getEncoded();
            byte[] privateKey = kp.getPrivate().getEncoded();
            String s1 = new String(publicKey);
            String s2 = new String(privateKey);
            System.out.println(">>key public ::" + s1);
            System.out.println(">>key private ::" + s2);

            //Generate CSR in PKCS#10 format encoded in DER
            PKCS10CertificationRequest csr = CSRHelper.generateCSR(kp, "naem");
            byte csrArray[] = csr.getEncoded();
            System.out.println(">>key CSR ::" + new String(csrArray));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
