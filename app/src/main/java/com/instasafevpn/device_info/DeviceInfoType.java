package com.instasafevpn.device_info;

public enum DeviceInfoType {
    OSNAME,
    OSVERSION,
    RELEASE,
    DEVICE,
    MODEL,
    PRODUCT,
    BRAND,
    DISPLAY,
    HARDWARE,
    ID,
    MANUFACTURER,
    SERIAL,
    USER,
    HOST,

    DEVICE_TYPE,
    DEVICE_SYSTEM_NAME,
    DEVICE_VERSION,
    DEVICE_SYSTEM_VERSION,
    DEVICE_MANUFACTURE,
    DEVICE_CURRENT_YEAR,
    DEVICE_CURRENT_DATE_TIME,
    DEVICE_HARDWARE_MODEL,
    DEVICE_NUMBER_OF_PROCESSORS,
    DEVICE_LOCALE,
    DEVICE_NETWORK,
    DEVICE_NETWORK_TYPE,
    DEVICE_IP_ADDRESS_IPV4,
    DEVICE_IP_ADDRESS_IPV6,
    DEVICE_MAC_ADDRESS,
    DEVICE_TOTAL_MEMORY,
    DEVICE_FREE_MEMORY,
    DEVICE_USED_MEMORY,
    DEVICE_IN_INCH,
    DEVICE_LANGUAGE,
    DEVICE_TIME_ZONE,
    DEVICE_LOCAL_COUNTRY_CODE
}
