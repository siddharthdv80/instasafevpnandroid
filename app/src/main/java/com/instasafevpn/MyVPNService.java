package com.instasafevpn;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.ParcelFileDescriptor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.TimeUnit;

public class MyVPNService extends VpnService {

    private Thread mThread;
    private ParcelFileDescriptor mInterface;
    //a. Configure a builder for the interface.
    Builder builder = new Builder();
    /**
     * Maximum packet size is constrained by the MTU, which is given as a signed short.
     */
    private static final int MAX_PACKET_SIZE = Short.MAX_VALUE;

    /**
     * Time to wait without receiving any response before assuming the server is gone.
     */
    private static final long RECEIVE_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20);
    /**
     * Time between polling the VPN interface for new traffic, since it's non-blocking.
     * <p>
     * TODO: really don't do this; a blocking read on another thread is much cleaner.
     */
    private static final long IDLE_INTERVAL_MS = TimeUnit.MILLISECONDS.toMillis(100);

    /**
     * Time between keepalives if there is no traffic at the moment.
     * <p>
     * TODO: don't do this; it's much better to let the connection die and then reconnect when
     * necessary instead of keeping the network hardware up for hours on end in between.
     **/
    private static final long KEEPALIVE_INTERVAL_MS = TimeUnit.SECONDS.toMillis(15);


    // Services interface
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println(">>Rock onStartCommand ::");
        // Start a new session by creating a new thread.
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    // -----------------Interface-----------------

                    //Only these apps will comes under VPN
//                    addAllowPackages(builder);

                    //Configure the TUN and get the interface.
                    mInterface = builder
                            .addAddress("10.50.0.3", 32)
                            .addDnsServer("8.8.8.8")
                            .addRoute("0.0.0.0", 0)
                            .establish();

                    //b. Packets to be sent are queued in this input stream.
                    FileInputStream in = new FileInputStream(
                            mInterface.getFileDescriptor());
                    //b. Packets received need to be written to this output stream.
                    FileOutputStream out = new FileOutputStream(
                            mInterface.getFileDescriptor());


                    //-------------------Peer-----------------------

                    //c. The UDP channel can be used to pass/get ip package to/from server
                    DatagramChannel tunnel = DatagramChannel.open();

                    InetSocketAddress socketAddress = new InetSocketAddress("3.101.128.28", 443);
                    tunnel.connect(socketAddress);

                    // For simplicity, we use the same thread for both reading and
                    // writing. Here we put the tunnel into non-blocking mode.
                    tunnel.configureBlocking(false);

                    //Protect this socket, so package send by it will not be feedback to the vpn service.
                    protect(tunnel.socket());

                    //Allocate the buffer for a single packet.
                    ByteBuffer packet = ByteBuffer.allocate(MAX_PACKET_SIZE);
//
//                    packet.put((byte) 0).put("PrtBg/5jooxYciQgi1XCzyGhhZtQejcjIzqmKQRtPSc=".getBytes()).flip();
//                    packet.position(0);
//
                    //Timeouts:
                    // - when data has not been sent in a while, send empty keepalive messages.
                    // - when data has not been received in a while, assume the connection is broken.
                    long lastSendTime = System.currentTimeMillis();
                    long lastReceiveTime = System.currentTimeMillis();
                    // We keep forwarding packets till something goes wrong.
                    while (true) {
                        // Assume that we did not make any progress in this iteration.
                        boolean idle = true;
                        // Read the outgoing packet from the input stream.
                        int length = in.read(packet.array());
                        if (length > 0) {
                            // Write the outgoing packet to the tunnel.
                            packet.limit(length);
                            tunnel.write(packet);
                            System.out.println(">>Tunnel Write ::" + length);
                            packet.clear();
                            // There might be more outgoing packets.
                            idle = false;
                            lastReceiveTime = System.currentTimeMillis();
                        }
                        // Read the incoming packet from the tunnel.
                        length = tunnel.read(packet);
                        System.out.println(">>Tunnel Read ::" + length);
                        if (length > 0) {
                            // Ignore control messages, which start with zero.
                            if (packet.get(0) != 0) {
                                // Write the incoming packet to the output stream.
                                out.write(packet.array(), 0, length);
                            }
                            packet.clear();
                            // There might be more incoming packets.
                            idle = false;
                            lastSendTime = System.currentTimeMillis();
                        }
                        // If we are idle or waiting for the network, sleep for a
                        // fraction of time to avoid busy looping.
                        if (idle) {
                            Thread.sleep(IDLE_INTERVAL_MS);
                            final long timeNow = System.currentTimeMillis();
                            if (lastSendTime + KEEPALIVE_INTERVAL_MS <= timeNow) {
                                // We are receiving for a long time but not sending.
                                // Send empty control messages.
                                packet.put((byte) 0).limit(1);
                                for (int i = 0; i < 3; ++i) {
                                    packet.position(0);
                                    tunnel.write(packet);
                                }
                                packet.clear();
                                lastSendTime = timeNow;
                            } else if (lastReceiveTime + RECEIVE_TIMEOUT_MS <= timeNow) {
                                // We are sending for a long time but not receiving.
                                throw new IllegalStateException("Timed out");
                            }
                        }
                    }

                } catch (Exception e) {
                    // Catch any exception
                    e.printStackTrace();
                    System.out.println(">>Rock Exception ::" + e.getMessage());
                } finally {
                    try {
                        if (mInterface != null) {
                            mInterface.close();
                            mInterface = null;
                        }
                    } catch (Exception e) {
                        System.out.println(">>Rock Exception22 ::" + e.getMessage());
                    }
                }
            }

        }, "MyVpnRunnable");

        //start the service
        mThread.start();
        return START_STICKY;
    }

//    private ParcelFileDescriptor configure(String trim) {
//        System.out.println(">>Str ::" + trim);
//        return null;
//    }

    private void addAllowPackages(Builder builder) {
        String[] appPackages = {
                "com.android.chrome",
                "com.google.android.youtube"};

        for (String appPackage : appPackages) {
            try {
                builder.addAllowedApplication(appPackage);
            } catch (PackageManager.NameNotFoundException e) {
                // The app isn't installed.
            }
        }
    }

    @Override
    public void onDestroy() {
        System.out.println(">>Rock onDestroy ::");
        // TODO Auto-generated method stub
        if (mThread != null) {
            mThread.interrupt();
        }
        super.onDestroy();
    }

}