package com.instasafevpn.events;

import com.instasafevpn.R;

public enum VPNState {
    STATE_REATTEMPT,
    STATE_DISCONNECTED(R.string.state_disconnected),
    STATE_CONNECTING(R.string.state_connecting),
    STATE_CONNECTED(R.string.state_connected),
    STATE_VPN_FAILED(R.string.state_failed);
    int stringID = -1;
    VPNState() {
    }

    VPNState(int stringId) {
        this.stringID = stringId;
    }

    public int getStringID() {
        return stringID;
    }
}
