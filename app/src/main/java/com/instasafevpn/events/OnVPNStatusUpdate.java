package com.instasafevpn.events;

public interface OnVPNStatusUpdate {
    VPNState getState();
}
