package com.instasafevpn.wirevpn

import android.content.Context
import com.instasafevpn.R
import com.wireguard.config.Config
import com.wireguard.config.Interface
import com.wireguard.config.Peer
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.ArrayList

class VPNConfig {
    companion object {
        private fun getAllowPackages(): HashSet<String> {
            val includeApps: HashSet<String> = HashSet()
//            includeApps.add("com.android.chrome")
            return includeApps
        }

        private fun getInterface(): Interface {
            val interfaceBuilder = Interface.Builder()
            interfaceBuilder.parseAddresses("10.50.0.3/32")
            interfaceBuilder.parseDnsServers("8.8.8.8")
            interfaceBuilder.parsePrivateKey("CLyw3fy/tIj7ypwJ+VWdSk/9pdVVWiSCdMe9Rpro3Vk=")
            interfaceBuilder.includeApplications(getAllowPackages())
            return interfaceBuilder.build()
        }

        private fun getPeer(): Peer {
            val builder = Peer.Builder()
            builder.parseEndpoint("3.101.128.28:443")
            builder.parseAllowedIPs("0.0.0.0")
            builder.parsePersistentKeepalive("25")
            builder.parsePublicKey("PrtBg/5jooxYciQgi1XCzyGhhZtQejcjIzqmKQRtPSc=")
            builder.parsePreSharedKey("RHNCSwp8/06DPzGIuCqGHxPOz3W17XFuTiOhJudwtmM=")
            return builder.build()
        }

        fun getConfig(): Config {
            val resolvedPeers: MutableCollection<Peer> = ArrayList()
            resolvedPeers.add(getPeer())
            return Config.Builder()
                    .setInterface(getInterface())
                    .addPeers(resolvedPeers)
                    .build()
        }

//        @Throws(IOException::class)
//        fun create(context: Context, name: String, config: Config): Config {
//            val file = fileFor(context, name)
//            if (!file.createNewFile())
//                throw IOException(context.getString(R.string.config_file_exists_error, file.name))
//            FileOutputStream(file, false).use { it.write(config.toWgQuickString().toByteArray(StandardCharsets.UTF_8)) }
//            return config
//        }

//        private fun fileFor(context: Context, name: String): File {
//            return File(context.filesDir, "$name.conf")
//        }
    }
}