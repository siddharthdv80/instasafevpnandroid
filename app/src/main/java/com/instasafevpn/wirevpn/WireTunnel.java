package com.instasafevpn.wirevpn;

import com.instasafevpn.events.OnVPNStatusUpdate;
import com.instasafevpn.events.VPNState;
import com.wireguard.android.backend.Tunnel;
import com.wireguard.config.Config;

import org.greenrobot.eventbus.EventBus;

public class WireTunnel implements Tunnel {
    private String mTunnelName;
    private Config mTunnelConfig;
    private State mCurrentState = State.DOWN;
    private int mCorrectionAttempts = 0;

    public WireTunnel(String tunnelName, Config tunnelConfig) {
        this.mTunnelName = tunnelName;
        this.mTunnelConfig = tunnelConfig;
    }

    @Override
    public String getName() {
        return mTunnelName;
    }

    @Override
    public void onStateChange(State newState) {
        mCurrentState = newState;
        if (newState == State.UP) {
            EventBus.getDefault().post((OnVPNStatusUpdate) () -> VPNState.STATE_CONNECTED);
        } else {
            EventBus.getDefault().post((OnVPNStatusUpdate) () -> VPNState.STATE_DISCONNECTED);
        }
        resetAttemptCount();
    }

    public Config getTunnelConfig() {
        return mTunnelConfig;
    }

    public boolean isRunning() {
        return mCurrentState == State.UP;
    }

    private void resetAttemptCount() {
        mCorrectionAttempts = 0;
    }

    public boolean canReAttempt() {
        if (mCorrectionAttempts < 2) {
            mCorrectionAttempts++;
            return true;
        }
        return false;
    }

}
