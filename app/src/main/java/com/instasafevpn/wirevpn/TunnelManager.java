package com.instasafevpn.wirevpn;

import com.instasafevpn.application.InstaSafeApplication;
import com.instasafevpn.events.OnVPNStatusUpdate;
import com.instasafevpn.events.VPNState;

import org.greenrobot.eventbus.EventBus;

public class TunnelManager {
    private WireTunnel wireTunnel;
    private static TunnelManager sTunnelManager;

    public static TunnelManager getInstance() {
        if (sTunnelManager == null) {
            sTunnelManager = new TunnelManager();
        }
        return sTunnelManager;
    }

    private void initTunnel() {
        if (wireTunnel == null) {
            wireTunnel = new WireTunnel("MyTunnel", VPNConfig.Companion.getConfig());
        }
    }

    public boolean isVPNConnected() {
        if (wireTunnel != null) {
            return wireTunnel.isRunning();
        }
        return false;
    }

    public void startStopVPN() {
        try {
            initTunnel();
            WireTunnel.State stateToSet = wireTunnel.isRunning() ? WireTunnel.State.DOWN : WireTunnel.State.UP;
            InstaSafeApplication.getInstance().getVPNBackend()
                    .setState(wireTunnel, stateToSet, wireTunnel.getTunnelConfig());
        } catch (Exception e) {
            e.printStackTrace();
            //For the first time VPN service might failed so we reattempt
            if (wireTunnel.canReAttempt()) {
                EventBus.getDefault().post((OnVPNStatusUpdate) () -> VPNState.STATE_REATTEMPT);
            } else {
                EventBus.getDefault().post((OnVPNStatusUpdate) () -> VPNState.STATE_VPN_FAILED);
            }
        }
    }
}
