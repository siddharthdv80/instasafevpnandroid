package com.inspironlabs.rest;

import com.inspironlabs.util.ValidationUtil;

public class ServiceSettings {

    private String mProtocol;
    private String mDomain;
    private String mPort;

    public ServiceSettings(String protocol, String domain, String port) {
        this.mProtocol = protocol;
        this.mDomain = domain;
        this.mPort = port;
    }

    String getServerUrl() {
        return mProtocol + "://" + mDomain + ((ValidationUtil.isEmpty(mPort) ? "" : ":" + mPort));
    }

}
