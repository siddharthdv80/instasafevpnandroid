package com.inspironlabs.rest;

public class ServerConfig {
    // Server URL
    public static final String PROTOCOL = "http";

    public static final String DOMAIN = "54.219.173.51";
    public static final String PORT = "9000";

    public static final String BASE_PATH = "v1/";
// https:virusiq.health:9000
// 54.219.173.51:9000
}
