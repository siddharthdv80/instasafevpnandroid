package com.inspironlabs.rest;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    private static final int CONNECT_TIMEOUT = 60000 * 12;
    private static final int READ_TIMEOUT = 60000 * 12;

    private ServiceSettings mServiceSettings;
    private OkHttpClient mOkHttpClient;
    private Retrofit mRetrofit;
    private Context context;

    public ServiceGenerator(Context context, ServiceSettings serviceSettings) {
        this.context = context;
        this.mServiceSettings = serviceSettings;
    }

    public <API> API getApi(Class<API> apiClass, Interceptor authInterceptor) {
        return getRetrofitAdapter(authInterceptor).create(apiClass);
    }

    public <API> API getApi(Class<API> apiClass) {
        return getRetrofitAdapter().create(apiClass);
    }


    private Retrofit getRetrofitAdapter(Interceptor authInterceptor) {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(mServiceSettings.getServerUrl())
                    .client(getOkHttpClient(authInterceptor))
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return mRetrofit;
    }

    private Retrofit getRetrofitAdapter() {
        if (mRetrofit == null) {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(mServiceSettings.getServerUrl())
                    .client(getOkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return mRetrofit;
    }

    private OkHttpClient getOkHttpClient(Interceptor authInterceptor) {
        if (mOkHttpClient == null) {
            mOkHttpClient = SelfSigningClientBuilder.createClient(context)
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .addInterceptor(createCustomInterceptor())
                    .addInterceptor(createLoggingInterceptor())
                    .addInterceptor(authInterceptor)
                    .build();
        }
        return mOkHttpClient;
    }

    private OkHttpClient getOkHttpClient() {
        if (mOkHttpClient == null) {
            mOkHttpClient = SelfSigningClientBuilder.createClient(context)
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .addInterceptor(createCustomInterceptor())
                    .addInterceptor(createLoggingInterceptor())
                    .build();
        }
        return mOkHttpClient;
    }

    private Interceptor createCustomInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        };
    }

    private Interceptor createLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }
}
