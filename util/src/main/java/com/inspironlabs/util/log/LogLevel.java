package com.inspironlabs.util.log;

/**
 * Argo project.
 */

public enum LogLevel {
    DEBUG,
    INFO,
    WARNING,
    ERROR;

    public boolean pass(LogLevel ll) {
        return this.ordinal() <= ll.ordinal();
    }

}
