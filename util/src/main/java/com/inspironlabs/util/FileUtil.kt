package com.inspironlabs.util

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import io.reactivex.Single
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.time.LocalDate
import java.time.Period

object FileUtil {

    private val APP_STORAGE_DIRECTORY_PATH = "/Orbit/Images"

    fun saveBitmap(context: Context, bitmap: Bitmap) : Single<File> {
        return Single.create { it ->
            val file = File(context.filesDir, "post_image.jpg")
            if (file.exists()) {
                file.delete()
            }
            var out: FileOutputStream? = null
            try {
                out = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                it.onSuccess(file)
            } catch (ex: Exception) {
                it.onError(ex)
            } finally {
                out?.close()
            }
        }
    }

    fun createFile(activity: Activity, fileName: String): File {
        var file = File(getFileDirectory(activity), fileName)
        if (file.exists()) {
            Log.d("TTTT", "File exists DeleteResult=${file.delete()} Exists=${file.exists()}")
        }
        file = File(getFileDirectory(activity), fileName)
        file.createNewFile()
        return file
    }

    private fun getFileDirectory(activity: Activity): File {
        val filePath = File(activity.filesDir, APP_STORAGE_DIRECTORY_PATH)
        if (!filePath.exists()) {
            val directoryCreated = filePath.mkdirs()
            check(directoryCreated) { "Error creating Image directory" }
        }
        return filePath
    }

}