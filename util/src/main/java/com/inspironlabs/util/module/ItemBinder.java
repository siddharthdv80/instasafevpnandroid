package com.inspironlabs.util.module;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

public interface ItemBinder<DATA> {

    void bindData(Activity activity, RecyclerView.ViewHolder holder, DATA data);

}
