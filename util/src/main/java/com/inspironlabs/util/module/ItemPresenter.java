package com.inspironlabs.util.module;

import android.app.Activity;

import androidx.recyclerview.widget.RecyclerView;

public interface ItemPresenter<DATA, VIEW_HOLDER extends RecyclerView.ViewHolder> {

    void bind(Activity activity, VIEW_HOLDER holder, DATA data);

}
