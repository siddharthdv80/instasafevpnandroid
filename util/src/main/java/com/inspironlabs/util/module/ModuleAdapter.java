package com.inspironlabs.util.module;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ModuleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity activity;
    private final List<Module> moduleList;
    private final LayoutInflater layoutInflater;

    public ModuleAdapter(Activity activity, List<Module> moduleList) {
        this.activity = activity;
        this.layoutInflater = LayoutInflater.from(activity);
        this.moduleList = moduleList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Module module = moduleList.get(0);
        module.presenter.bind(activity, holder, module.data);
    }

    @Override
    public int getItemCount() {
        return moduleList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return moduleList.get(position).moduleType;
    }
}
