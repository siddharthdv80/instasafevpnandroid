package com.inspironlabs.util.module;

public class Module<DATA, PRESENTER extends ItemPresenter> {

    public int moduleType;
    public DATA data;
    public PRESENTER presenter;

    public Module(int moduleType, DATA data, PRESENTER presenter) {
        this.moduleType = moduleType;
        this.data = data;
        this.presenter = presenter;
    }
}
