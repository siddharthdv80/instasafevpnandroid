package com.inspironlabs.util.module;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

//import com.inspironlabs.util.module.ex.ExamTitleItemBinder;
//import com.inspironlabs.util.module.ex.ExamTitlePresenter;
//import com.inspironlabs.util.module.ex.Subject;
//import com.inspironlabs.util.module.ex.SubjectItemBinder;
//import com.inspironlabs.util.module.ex.SubjectPresenter;
//import com.inspironlabs.util.module.ex.data.ExamsAndSubjectDetails;
//import com.inspironlabs.util.module.ex.data.ExamsAndSubjectsResponse;
//import com.inspironlabs.util.module.ex.data.SubjectDetails;

import java.util.ArrayList;
import java.util.List;

public class ModuleFactory {

//    public static ItemPresenter createPresenter(int moduleType, ItemBinder itemBinder) {
//        switch (moduleType) {
//            case ModuleType.EXAM_TITLE:
//                return new ExamTitlePresenter(itemBinder);
//            case ModuleType.SUBJECTS:
//                return new SubjectPresenter(itemBinder);
//            default:
//                throw new RuntimeException("Unknown module type " + moduleType);
//        }
//    }
//
//    public static ItemBinder createBinder(int moduleType) {
//        switch (moduleType) {
//            case ModuleType.EXAM_TITLE:
//                return new ExamTitleItemBinder();
//            case ModuleType.SUBJECTS:
//                return new SubjectItemBinder();
//            default:
//                throw new RuntimeException("Unknown module type " + moduleType);
//        }
//    }
//
//    public static RecyclerView.ViewHolder createViewHolder(int moduleType, ViewGroup parent, LayoutInflater layoutInflater) {
//        switch (moduleType) {
//            case ModuleType.EXAM_TITLE:
//                // Todo: Set the item id our here
//                View header = layoutInflater.inflate(1, parent, false);
//                return new ExamTitlePresenter.ExamTitleViewHolder(header);
//            case ModuleType.SUBJECTS:
//                // Todo: Set the item id our here
//                View subjct = layoutInflater.inflate(1, parent, false);
//                return new SubjectPresenter.SubjectViewHolder(subjct);
//            default:
//                throw new RuntimeException("Unknown module type " + moduleType);
//        }
//    }
//
//    public static List<Module> toModules(ExamsAndSubjectsResponse data) {
//        List<Module> moduleList = new ArrayList<>();
//
//        ItemBinder examTitleItemBinder = createBinder(ModuleType.EXAM_TITLE);
//        ItemPresenter examTitlePresenter = createPresenter(ModuleType.EXAM_TITLE, examTitleItemBinder);
//
//        ItemBinder subjectItemBinder = createBinder(ModuleType.SUBJECTS);
//        ItemPresenter subjectPresenter = createPresenter(ModuleType.SUBJECTS, subjectItemBinder);
//
//        for (ExamsAndSubjectDetails exam : data.response) {
//            // Add Exam Header
//            moduleList.add(new Module(ModuleType.EXAM_TITLE, exam.exam, examTitlePresenter));
//
//            // Add Subject
//            // We have to add to two subject in a item
//            int subjectCount = exam.subjects.size();
//            List<SubjectDetails> subject = new ArrayList<>();
//            for (int i = 0; i < subjectCount; i += 2) {
//                subject.add(exam.subjects.get(i));
//                subject.add(exam.subjects.get(i + 1));
//            }
//            moduleList.add(new Module(ModuleType.SUBJECTS, subject, subjectPresenter));
//        }
//        return moduleList;
//    }

}
