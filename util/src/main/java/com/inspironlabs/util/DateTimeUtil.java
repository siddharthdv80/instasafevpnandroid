package com.inspironlabs.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil {

    private final static SimpleDateFormat ddMMyyyyFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
    private final static SimpleDateFormat yyyyMMddFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    public static String formatToDDMMYYYY(Date date) {
        return ddMMyyyyFormat.format(date);
    }

    public static String formatToYYYYMMDD(Date date) {
        return yyyyMMddFormat.format(date);
    }

}
